
library bible.penums;

enum BBLocale {
  EN,
  ES,
  FR,
  IT,
  PT,
  DE
}

enum TopBarMenuType {
  NONE,
  BPA,
  MULTI,
  LANGUAGE
}

enum ClipboardAddType {
  VERSE,
  CHAPTER
}

enum SelectionType {
  SINGLE,
  MULTI,
  ALT
}

enum HighlightType {
  NO_STYLE,
  FILLED,
  SQUARED
}

/*
enum ChapterViewType {
  LIST, GRID
}
*/
