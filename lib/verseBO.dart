library bible.verse_bo;

class VerseBO {
  final int id;
  final int bbName;
  final int bNumber;
  final int cNumber;
  final int vNumber;
  final String vText;

  VerseBO(
      {this.id,
      this.bbName,
      this.bNumber,
      this.cNumber,
      this.vNumber,
      this.vText});
}
