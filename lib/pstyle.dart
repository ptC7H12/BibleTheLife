library bible.pstyle;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PStyle {
  //Singleton
  PStyle._privateConstructor();
  static final PStyle instance = PStyle._privateConstructor();

  /* Common style properties */
  final String emojiFamily = "NotoColorEmoji";
  String fontFamily = "Droid-sans.regular";
  double fontSize = 20.0;
  get fontSizeForIconInPage => 20.0;

  //TODO: MENU, we will use the selected font family for Menus
  String fontFamilyForMenu() => "Droid-sans.regular";

  //TODO: MENU, minimal height
  double fontSizeForMenu({ double max }) {
    if (max == null) max = 20.0;
    final double sizeMin = 16.0;
    final double sizeMax = max < sizeMin ? 20.0 : max;
    final fontSizeMenu = (fontSize > sizeMax)
        ? sizeMax
        : (fontSize < sizeMin)
        ? sizeMin
        : fontSize;
    return fontSizeMenu;
  }

  ///Get verse style used
/*Future<VerseStyle> getVerseStyle() async {
  final String themeName = await P.Prefs.getThemeName;
  return Future.value(themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle());
}*/

  ///Get verse style used
  VerseStyle getVerseStyle(final String themeName) {
    return themeName.compareTo('LIGHT') == 0 ? LightVerseStyle() : DarkVerseStyle();
  }

  //Official Dark
  final ThemeData darkThemeData = ThemeData(
    primarySwatch: Colors.blue,
    //primarySwatch: Colors.deepOrange,
    accentColor: Colors.blueAccent,
    //accentColor: Colors.deepOrangeAccent,
    brightness: Brightness.dark,
  );

  //TEST Light less agressive!
  /*final ThemeData lightThemeData = ThemeData(
    primarySwatch: Colors.red,
    accentColor: Colors.blueAccent,
    //based on < 0.5: brightness: ColorScheme.lerp(ColorScheme.light().copyWith(), ColorScheme.dark().copyWith(), 0.6).brightness,
  );
   */

  //Official Light
  final ThemeData lightThemeData = ThemeData(
    primarySwatch: Colors.blue,
    accentColor: Colors.blueAccent,
    brightness: Brightness.light,
    //colorScheme: ColorScheme.light(onBackground: Colors.grey, brightness: Brightness.light),
  );
}

  /* Themes */
  /*
  //TEST
  final ThemeData darkThemeData = ThemeData.dark().copyWith(
    accentColor: Colors.blueAccent, //accentColor: Colors.deepOrangeAccent,
    scaffoldBackgroundColor: Colors.black26,
    appBarTheme: AppBarTheme(elevation: 1.0, brightness: Brightness.light),
    buttonTheme: ButtonThemeData(hoverColor: Colors.deepOrange),
  );*/

/* Styles */
abstract class VerseStyle {
  Color defaultColor;
  Color inverseDefaultColor;
  Color bookNameColor;
  Color chapterVnumberColor;
  Color crColor;
  Color accentColor;
  Color fgColor1; //alt 1
  Color bgColor1; //alt 1
}

//Official LightVerseStyle
class LightVerseStyle implements VerseStyle {
  Color defaultColor = Colors.black;
  Color inverseDefaultColor = Colors.white;
  Color bookNameColor = Colors.blue;
  Color chapterVnumberColor = Colors.blue;
  Color crColor = Colors.red;
  Color accentColor = Colors.blue;
  Color fgColor1 = const Color(0xFF757779); //other languages brown:0xFF662B00, dark gray:0xFF3d3f41
  Color bgColor1 = ThemeData.light().backgroundColor; //other languages (not used currently)
}

//Official DarkVerseStyle
class DarkVerseStyle implements VerseStyle {
  Color defaultColor = Colors.white;
  Color inverseDefaultColor = Colors.black;
  Color bookNameColor = Colors.blue;
  Color chapterVnumberColor = Colors.blue;
  Color crColor = Colors.red;
  Color accentColor = Colors.blue;
  Color fgColor1 = Colors.grey;
  Color bgColor1 = ThemeData.dark().backgroundColor;
}
