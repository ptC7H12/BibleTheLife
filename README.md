## Description
Bible multi languages, free, offline, no advertising, in English, French, Italian, Spanish, Portuguese, German.  

The application uses a modern and clean interface.  

Easy to use with quick searches and shares, favorites, parables, articles, cross-references, but also includes several fonts for people having visibility impairs and a rich clipboard functionality allowing you to copy several verses and chapters of different books before sharing the result.  

You can browse your search history (containing opened books, parables, cross references...) and let you navigate through in an infinite way.  

The Life is a powerful study tool to learn the Word of God.  

Bibles included: King James Version (in old and modern English), Segond, Ostervald, Diodati, Valera, Almeida, Schlachter.  

For Android, iPhone, iPad, Big Sur, Mac and Linux.  
Please share the info with your friends.  
Time is short. Tribulations are at the door.  

• iPhone, iPad, Big Sur:  
https://apps.apple.com/us/app/bible-multi-the-life/id1502832136

• Mac:  
https://apps.apple.com/us/app/bible-multi-the-life/id1587163844

• Linux:  
https://snapcraft.io/bible-multi-the-life

• Google:  
https://play.google.com/store/apps/details?id=org.hlwd.bible_multi_the_life

• F-Droid:  
https://f-droid.org/en/packages/org.hlwd.bible_multi_the_life

** All The Glory To God.
  
  
  
## License
<img src="https://gnu.org/graphics/gplv3-127x51.png" />
[GPLv3](http://www.gnu.org/licenses/gpl-3.0.html) 
  
  
  
## Screenshots
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb1.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb2.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb3.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb4.png)
![Screenshot](/fastlane/metadata/android/en-US/images/phoneScreenshots/glb5.png)
![Screenshot](/fastlane/metadata/android/en-US/images/sevenInchScreenshots/glb6.png)
![Screenshot](/fastlane/metadata/android/en-US/images/sevenInchScreenshots/glb7.png)
![Screenshot](/fastlane/metadata/android/en-US/images/sevenInchScreenshots/glb8.png)
