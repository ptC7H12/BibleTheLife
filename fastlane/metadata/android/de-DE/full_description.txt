Mehrsprachige Bibel, kostenlos, offline, ohne Werbung, ganz in Englisch, Französisch, Italienisch, Spanisch, Portugiesisch, Deutsch.

Die App verwendet eine moderne und saubere Schnittstelle.

Einfach zu bedienen mit schnellen Suchen und Teilen, Favoriten, Parabeln, Artikeln, Querverweisen, aber auch mit mehreren Schriften für Personen mit geringer Sichtbarkeit und reichhaltiger PressefunktionalitätSie können mehrere Verse und Kapitel aus verschiedenen Büchern kopieren, bevor Sie das Ergebnis teilen.

Sie können Ihre Suchhistorie (mit offenen Büchern, Parabeln, Querverweisen...) durchsuchen und Sie endlos navigieren lassen.

The Life ist ein mächtiges Lernwerkzeug, um das Wort Gottes zu lernen.

Inklusive Bibel: King James Version (Englisch alt und modern), Segond, Ostervald, Diodati, Valera, Almeida, Schlachter.

Für Android, iPhone, iPad, Big Sur, Mac.
Bitte teilen Sie die Info mit Ihren Freunden.
Die Zeit ist kurz. Leiden sind vor unserer Haustür.

** All The Glory To God.
