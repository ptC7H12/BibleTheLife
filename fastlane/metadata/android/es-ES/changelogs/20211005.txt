• Mejor rendimiento
• Soporte de visualización para personas con discapacidad visual: fuentes grandes admitidas
• Arrastre a la izquierda/derecha (swipe) para cambiar la página
• Búsqueda en las Biblias mostradas
• Soporte: MacOS
