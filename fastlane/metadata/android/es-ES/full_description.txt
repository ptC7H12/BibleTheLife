Biblia en varios idiomas, gratuita offline, sin publicidad, integramente en Inglés, francés, italiano, español, portugués, aléman.

La aplicación utiliza una interfaz moderna y limpia.

Fácil de usar con búsquedas rápidas y compartidos, favoritos, parábolas, artículos, referencias cruzadas, pero también incluye varias fuentes para personas con problemas de visibilidad y una rica funcionalidad de portapapeles que le permite copiar varios versos y capítulos de diferentes libros antes de compartir el resultado.

Puedes navegar por tu historial de búsqueda (que contiene libros abiertos, parábolas, referencias cruzadas...) y dejarte navegar de una manera infinita.

The Life es una poderosa herramienta de estudio para aprender la Palabra de Dios.

Biblias incluidas: King James Version (en inglés antiguo y moderno), Segond, Ostervald, Diodati, Valera, Almeida, Schlachter.

Para Android, iPhone, iPad, Big Sur, Mac.
Por favor, comparta la información con sus amigos.
El tiempo es corto. Las tribulaciones están en nuestra puerta.

** All The Glory To God.
